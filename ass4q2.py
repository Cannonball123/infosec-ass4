from scapy.all import *
from multiprocessing.dummy import Pool as ThreadPool
import threading
import logging
import sys 


isRange = 0;

def portScan(dst_port):
	src_port = RandShort()
	logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
	stealth_scan_resp = sr1(IP(dst=dst_ip)/TCP(sport=src_port,dport=dst_port,flags="S"),timeout=10)

	if(str(type(stealth_scan_resp))=="<type 'NoneType'>"):
		return "Destination ip ", dst_ip, ", Port ", dst_port, ": Filtered"

	elif(stealth_scan_resp.haslayer(TCP)):	
		if(stealth_scan_resp.getlayer(TCP).flags == 0x12):
			send_rst = sr(IP(dst=dst_ip)/TCP(sport=src_port,dport=dst_port,flags="R"),timeout=10)
			return "Destination ip ", dst_ip, ", Port ", dst_port, ": Open"

		elif (stealth_scan_resp.getlayer(TCP).flags == 0x14):
			return "Destination ip ", dst_ip, ", Port ", dst_port, ": Closed"

	elif(stealth_scan_resp.haslayer(ICMP)):
		if(int(stealth_scan_resp.getlayer(ICMP).type)==3 and int(stealth_scan_resp.getlayer(ICMP).code) in [1,2,3,9,10,13]):
			return "Destination ip ", dst_ip, ", Port ", dst_port, ": Filtered"

def parallelScan(ports, threads):
    pool = ThreadPool(threads)
    results = pool.map(portScan, ports)
    pool.close()
    pool.join()
    return results


cmdargs = sys.argv

# print cmdargs

dst_ip = cmdargs[1]
src_port = RandShort()
dst_ports = []

if("-" in cmdargs[2]):
	temp = cmdargs[2]
	dst_ports = range(int(temp.split('-')[0]), int(temp.split('-')[1])+1)
	isRange = 1
else:
	dst_ports = map(int , cmdargs[2 : len(cmdargs)])

stealtScanResults = parallelScan(dst_ports, 200)

print "==============================================================="
print "==============================================================="
print "==================== STEALTH SCAN RESULTS: ===================="
print "==============================================================="
print "==============================================================="

# for i in range(0, len(stealtScanResults)):
	# print '\n', stealtScanResults[i][0], stealtScanResults[i][1], stealtScanResults[i][2], stealtScanResults[i][3], stealtScanResults[i][4]

index = 0;
base_port_status = stealtScanResults[0][4]
acc_list = ""

if(isRange == 0):
	for i in range(1, len(stealtScanResults)):
		if(base_port_status != stealtScanResults[i][4]):
			if((i-index) <= 1):
				print stealtScanResults[i-1][0], stealtScanResults[i-1][1], "Port ", stealtScanResults[i-1][3] , stealtScanResults[i-1][4]
				index = i
				base_port_status = stealtScanResults[i][4]
				acc_list = acc_list + str(stealtScanResults[i][3]) + " "
			else:
				print stealtScanResults[i-1][0], stealtScanResults[i-1][1], "Ports ", acc_list , stealtScanResults[i-1][4]
				acc_list = ""
				index = i
				base_port_status = stealtScanResults[i][4]
		else:
			acc_list = acc_list + str(stealtScanResults[i][3]) + " "
		if(i == len(stealtScanResults)-1):
			if((i-index) <= 1):
				print stealtScanResults[i][0], stealtScanResults[i][1], "Port ", stealtScanResults[i][3] , stealtScanResults[i][4]
			else:
				print stealtScanResults[i-1][0], stealtScanResults[i-1][1], "Ports ", acc_list , stealtScanResults[i-1][4]

else:
	for i in range(1, len(stealtScanResults)):
		# print "\nstealtScanResults[i][4] is: ", stealtScanResults[i][4]
		# print "base_port_status is: ", base_port_status
		# print "index is: ", index
		if(base_port_status != stealtScanResults[i][4]):
			if((i-index) <= 1):
				print stealtScanResults[i-1][0], stealtScanResults[i-1][1], "Port ", stealtScanResults[i-1][3] , stealtScanResults[i-1][4]
				index = i
				base_port_status = stealtScanResults[i][4]
			else:
				print stealtScanResults[i-1][0], stealtScanResults[i-1][1], "Ports ", stealtScanResults[index][3], "-", stealtScanResults[i-1][3] , stealtScanResults[i-1][4]
				index = i
				base_port_status = stealtScanResults[i][4]
		if(i == len(stealtScanResults)-1):
			if((i-index) <= 1):
				print stealtScanResults[i][0], stealtScanResults[i][1], "Port ", stealtScanResults[i][3] , stealtScanResults[i][4]
			else:
				print stealtScanResults[i][0], stealtScanResults[i][1], "Ports ", stealtScanResults[index][3], "-", stealtScanResults[i][3] , stealtScanResults[i][4]


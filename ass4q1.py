from scapy.all import *

flags = {
    'F': 'FIN',
    'S': 'SYN',
    'R': 'RST',
    'P': 'PSH',
    'A': 'ACK',
    'U': 'URG',
    'E': 'ECE',
    'C': 'CWR',
}

def flagsFormat(p):
	packetFlags = []
	for x in p.sprintf('%TCP.flags%'):
		packetFlags.append(flags[x])
	return packetFlags

def printPayload(string):
    for i in range(0, len(string)):
    	if(string[i]<' ' or string[i]>'~'):
    		string = string[:i] + "." + string[i+1:]
    return string

def packetDetails(x):
	if(x.haslayer(IP)):
		if (x[IP].proto == 6):
			if(x.haslayer(Raw)):
				print '\n\nLength:',x[IP].len, '\nType: TCP', '\nMAC src: ', x.src, '\nMAC dst: ', x.dst, '\nIP src: ', x[IP].src, '\nIP dst: ', x[IP].dst, '\nTCP src port: ', x[TCP].sport, '\nTCP dst port: ', x[TCP].dport, '\nTCP flags: ', flagsFormat(x), '\nTCP payload: ', printPayload(x[Raw].load)
			else:
				print '\n\nLength:',x[IP].len, '\nType: TCP', '\nMAC src: ', x.src, '\nMAC dst: ', x.dst, '\nIP src: ', x[IP].src, '\nIP dst: ', x[IP].dst, '\nTCP src port: ', x[TCP].sport, '\nTCP dst port: ', x[TCP].dport, '\nTCP flags: ', flagsFormat(x)
		elif (x[IP].proto == 17):
			if(x.haslayer(Raw)):
				print '\n\nLength:',x[IP].len, '\nType: ==================================UDP', '\nMAC src: ', x.src, '\nMAC dst: ', x.dst, '\nIP src: ', x[IP].src, '\nIP dst: ', x[IP].dst, '\nUDP src port: ', x[UDP].sport, '\nUDP dst port: ', x[UDP].dport, '\nUDP payload: ', printPayload(x[Raw].load)
			else:
				print '\n\nLength:',x[IP].len, '\nType: ==================================UDP', '\nMAC src: ', x.src, '\nMAC dst: ', x.dst, '\nIP src: ', x[IP].src, '\nIP dst: ', x[IP].dst, '\nUDP src port: ', x[UDP].sport, '\nUDP dst port: ', x[UDP].dport
		elif (x[IP].proto == 1):
			print '\n\nLength:',x[IP].len, '\nType: =====================================================================ICMP', '\nMAC src: ', x.src, '\nMAC dst: ', x.dst, '\nIP src: ', x[IP].src, '\nIP dst: ', x[IP].dst, '\nICMP Type: ', x[ICMP].type, '\nICMP Subtype: ', x[ICMP].code
		else: 
			print '\n\nLength:',x[IP].len, '\nType: other', '\nMAC src: ', x.src, '\nMAC dst: ', x.dst, '\nIP src: ', x[IP].src, '\nIP dst: ', x[IP].dst

sniff(prn=lambda x: packetDetails(x))

print "Sniffing terminated."
